﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ServerItem
    Inherits System.Windows.Forms.UserControl

    'UserControl 重写释放以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。  
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ServerItem))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LinkLabelHomepage = New System.Windows.Forms.LinkLabel()
        Me.ButtonDelete = New System.Windows.Forms.Button()
        Me.LabelLogon = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ButtonEdit = New System.Windows.Forms.Button()
        Me.ButtonPing = New System.Windows.Forms.Button()
        Me.ButtonSet = New System.Windows.Forms.Button()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.LabelVersion = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.LabelPath = New System.Windows.Forms.Label()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(3, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(68, 68)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(72, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Logon Server:"
        '
        'LinkLabelHomepage
        '
        Me.LinkLabelHomepage.Location = New System.Drawing.Point(176, 28)
        Me.LinkLabelHomepage.Name = "LinkLabelHomepage"
        Me.LinkLabelHomepage.Size = New System.Drawing.Size(181, 17)
        Me.LinkLabelHomepage.TabIndex = 2
        Me.LinkLabelHomepage.TabStop = True
        Me.LinkLabelHomepage.Text = "LinkLabelHomepage"
        '
        'ButtonDelete
        '
        Me.ButtonDelete.BackColor = System.Drawing.Color.White
        Me.ButtonDelete.BackgroundImage = CType(resources.GetObject("ButtonDelete.BackgroundImage"), System.Drawing.Image)
        Me.ButtonDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ButtonDelete.Location = New System.Drawing.Point(360, 3)
        Me.ButtonDelete.Margin = New System.Windows.Forms.Padding(0)
        Me.ButtonDelete.Name = "ButtonDelete"
        Me.ButtonDelete.Size = New System.Drawing.Size(30, 30)
        Me.ButtonDelete.TabIndex = 3
        Me.ToolTip1.SetToolTip(Me.ButtonDelete, "删除/Delete")
        Me.ButtonDelete.UseVisualStyleBackColor = False
        '
        'LabelLogon
        '
        Me.LabelLogon.Location = New System.Drawing.Point(176, 3)
        Me.LabelLogon.Name = "LabelLogon"
        Me.LabelLogon.Size = New System.Drawing.Size(181, 17)
        Me.LabelLogon.TabIndex = 1
        Me.LabelLogon.Text = "Logon Server:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(72, 28)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(62, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Homepage:"
        '
        'ButtonEdit
        '
        Me.ButtonEdit.BackColor = System.Drawing.Color.White
        Me.ButtonEdit.BackgroundImage = CType(resources.GetObject("ButtonEdit.BackgroundImage"), System.Drawing.Image)
        Me.ButtonEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ButtonEdit.Location = New System.Drawing.Point(394, 3)
        Me.ButtonEdit.Margin = New System.Windows.Forms.Padding(0)
        Me.ButtonEdit.Name = "ButtonEdit"
        Me.ButtonEdit.Size = New System.Drawing.Size(30, 30)
        Me.ButtonEdit.TabIndex = 4
        Me.ToolTip1.SetToolTip(Me.ButtonEdit, "编辑/Edit")
        Me.ButtonEdit.UseVisualStyleBackColor = False
        '
        'ButtonPing
        '
        Me.ButtonPing.BackColor = System.Drawing.Color.White
        Me.ButtonPing.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ButtonPing.Location = New System.Drawing.Point(360, 33)
        Me.ButtonPing.Margin = New System.Windows.Forms.Padding(0)
        Me.ButtonPing.Name = "ButtonPing"
        Me.ButtonPing.Size = New System.Drawing.Size(64, 36)
        Me.ButtonPing.TabIndex = 5
        Me.ButtonPing.Text = "Ping"
        Me.ToolTip1.SetToolTip(Me.ButtonPing, "Ping")
        Me.ButtonPing.UseVisualStyleBackColor = False
        '
        'ButtonSet
        '
        Me.ButtonSet.BackColor = System.Drawing.Color.White
        Me.ButtonSet.BackgroundImage = CType(resources.GetObject("ButtonSet.BackgroundImage"), System.Drawing.Image)
        Me.ButtonSet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ButtonSet.Location = New System.Drawing.Point(424, 3)
        Me.ButtonSet.Margin = New System.Windows.Forms.Padding(0)
        Me.ButtonSet.Name = "ButtonSet"
        Me.ButtonSet.Size = New System.Drawing.Size(72, 66)
        Me.ButtonSet.TabIndex = 7
        Me.ToolTip1.SetToolTip(Me.ButtonSet, "启动/Launch WOW")
        Me.ButtonSet.UseVisualStyleBackColor = False
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(-1, 91)
        Me.ProgressBar1.Margin = New System.Windows.Forms.Padding(0)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(511, 19)
        Me.ProgressBar1.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(72, 52)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Version:"
        '
        'LabelVersion
        '
        Me.LabelVersion.Location = New System.Drawing.Point(176, 52)
        Me.LabelVersion.Name = "LabelVersion"
        Me.LabelVersion.Size = New System.Drawing.Size(181, 17)
        Me.LabelVersion.TabIndex = 9
        Me.LabelVersion.Text = "Logon Server:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(72, 72)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(66, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "WOWfolder:"
        '
        'LabelPath
        '
        Me.LabelPath.Location = New System.Drawing.Point(176, 71)
        Me.LabelPath.Name = "LabelPath"
        Me.LabelPath.Size = New System.Drawing.Size(181, 17)
        Me.LabelPath.TabIndex = 9
        Me.LabelPath.Text = "Path"
        '
        'ServerItem
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.White
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.LabelPath)
        Me.Controls.Add(Me.LabelVersion)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ButtonSet)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.ButtonPing)
        Me.Controls.Add(Me.ButtonEdit)
        Me.Controls.Add(Me.ButtonDelete)
        Me.Controls.Add(Me.LinkLabelHomepage)
        Me.Controls.Add(Me.LabelLogon)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Name = "ServerItem"
        Me.Size = New System.Drawing.Size(510, 99)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Label1 As Label
    Friend WithEvents LinkLabelHomepage As LinkLabel
    Friend WithEvents ButtonDelete As Button
    Friend WithEvents LabelLogon As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents ButtonEdit As Button
    Friend WithEvents ButtonPing As Button
    Friend WithEvents ProgressBar1 As ProgressBar
    Friend WithEvents ButtonSet As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents LabelVersion As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents LabelPath As Label
End Class
