﻿Imports System.Diagnostics
Imports System.Net.NetworkInformation
Imports System.Threading
Imports System.Runtime.InteropServices
Imports System.IO

Public Class ServerItem
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles ButtonDelete.Click
        If MsgBox($"确定删除？{vbLf}Are you sure to delete?", vbYesNo) = vbYes Then
            Me.Parent.Controls.Remove(Me)
            writelist()
        End If

    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabelHomepage.LinkClicked
        System.Diagnostics.Process.Start(LinkLabelHomepage.Text)
    End Sub

    Private Sub ButtonPing_Click(sender As Object, e As EventArgs) Handles ButtonPing.Click
        For i = 1 To 4
            ProgressBar1.Value = i * 100 / 4
            Using p As New Ping
                ButtonPing.Text = (p.Send(LabelLogon.Text).RoundtripTime.ToString() + "ms" + vbLf)

            End Using
        Next

    End Sub

    Private Sub ServerItem_MouseHover(sender As Object, e As EventArgs) Handles MyBase.MouseHover
        Me.BackColor = Color.AliceBlue
    End Sub

    Private Sub ServerItem_MouseLeave(sender As Object, e As EventArgs) Handles MyBase.MouseLeave
        Me.BackColor = Color.White
    End Sub

    Private Sub ButtonEdit_Click(sender As Object, e As EventArgs) Handles ButtonEdit.Click
        'Me.Parent.Controls.Remove(Me)
        Editor.TextBoxLogon.Text = Me.LabelLogon.Text
        Editor.TextBoxHome.Text = Me.LinkLabelHomepage.Text
        Editor.TextBoxVersion.Text = Me.LabelVersion.Text
        Editor.TextBoxFolder.Text = Me.LabelPath.Text
        Editor.TopMost = True
        Editor.Show()

    End Sub

    Private Sub ButtonSet_Click(sender As Object, e As EventArgs) Handles ButtonSet.Click
        If Directory.Exists(Me.LabelPath.Text) Then
            For Each realm In realmwtf
                If Directory.Exists(Me.LabelPath.Text & "\" & realm) Then
                    Dim sr As StreamWriter = New StreamWriter(Me.LabelPath.Text & "\" & realm & "\realmlist.wtf")
                    sr.WriteLine($"set realmlist {LabelLogon.Text}")
                    sr.Close()
                    sr = Nothing
                End If

            Next
            If Directory.Exists(Me.LabelPath.Text & "\WDB") Then
                Directory.Delete(Me.LabelPath.Text & "\WDB", True)
            End If
            If Directory.Exists(Me.LabelPath.Text & "\Cache") Then
                Directory.Delete(Me.LabelPath.Text & "\Cache", True)
            End If
            System.Diagnostics.Process.Start(Me.LabelPath.Text & "\wow.exe")
        Else
            MsgBox($"请将此文件放在魔兽世界目录{vbLf}please put this tool into wow folder")
        End If
    End Sub

    Private Sub ServerItem_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class
