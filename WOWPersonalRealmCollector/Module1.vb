﻿Imports System.IO

Module Module1
    Public reamdb As String = Application.StartupPath & "\realmlist.db"

    'realmlist.wtf
    Public realmwtf As String() = {"\",
        "\Data\zhTW",
         "\Data\enTW",
         "\Data\zhCN",
        "\Data\enCN",
        "\Data\enUS"}

    'Public reamwtf As String = Application.StartupPath & "\realmlist.wtf"



    'Public wowpath As String = Application.StartupPath & "\wow.exe"

    Public Sub writelist()
        '保存
        Dim strcontent As String = "logon" & vbTab & "homepage"
        For Each ctrl As ServerItem In Form1.FlowLayoutPanel1.Controls
            Dim line As String = ""
            line = ctrl.LabelLogon.Text & vbTab & ctrl.LinkLabelHomepage.Text & vbTab & ctrl.LabelVersion.Text & vbTab & ctrl.LabelPath.Text
            strcontent = strcontent & vbLf & line

        Next
        Dim sw As StreamWriter = New StreamWriter(reamdb)

        sw.Write(strcontent)
        sw.Close()
        sw = Nothing
    End Sub

    Public Sub readlist()
        '加载
        Dim sr As StreamReader = New StreamReader(Application.StartupPath & "\realmlist.db")
        Dim content As String = sr.ReadToEnd()

        Dim arrContent() As String = content.Split(vbLf)
        If arrContent.Count > 1 Then
            For i = 1 To arrContent.Count - 1 '0 is header
                Dim line() As String = arrContent(i).Split(vbTab)
                Dim it As New ServerItem

                it.LabelLogon.Text = line(0) ' "logon.stswow.com"
                it.LinkLabelHomepage.Text = line(1)  '"www.stswow.com"
                it.LabelVersion.Text = line(2)  '"1.12"
                it.LabelPath.Text = line(3)  '"D:\WOW\"
                it.PictureBox1.ImageLocation = "http://" & line(1) & "/favicon.ico"

                'Application.DoEvents()

                'it.PictureBox1.Image = Image.FromFile("http://" & line(1) & "/favicon.ico")
                Form1.FlowLayoutPanel1.Controls.Add(it)
            Next
        End If
        sr.Close()
        sr = Nothing
    End Sub
End Module
