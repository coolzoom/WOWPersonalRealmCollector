﻿Imports System.IO

Public Class Editor
    Private logon As String
    Private homepage As String
    Private version As String
    Private folder As String
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If TextBoxLogon.Text.Contains("http") Or TextBoxHome.Text.Contains("http") Then
            MsgBox("地址请不要带http. Please do not include http into the address")
        Else
            'edit
            If logon <> "" Or homepage <> "" Then
                For Each ctrl As ServerItem In Form1.FlowLayoutPanel1.Controls
                    'if is one of them, then change to new
                    If logon = ctrl.LabelLogon.Text Or homepage = ctrl.LinkLabelHomepage.Text Then
                        ctrl.LabelLogon.Text = TextBoxLogon.Text
                        ctrl.LinkLabelHomepage.Text = TextBoxHome.Text
                        ctrl.LabelVersion.Text = TextBoxVersion.Text
                        ctrl.LabelPath.Text = TextBoxFolder.Text
                    End If
                Next
                writelist()
                Me.Dispose()
            Else 'new item
                'new


                Dim ls As New ServerItem

                ls.LabelLogon.Text = TextBoxLogon.Text
                ls.LinkLabelHomepage.Text = TextBoxHome.Text
                ls.LabelVersion.Text = TextBoxVersion.Text
                ls.LabelPath.Text = TextBoxFolder.Text
                ls.PictureBox1.ImageLocation = "http://" & ls.LinkLabelHomepage.Text & "/favicon.ico"
                Form1.FlowLayoutPanel1.Controls.Add(ls)

                writelist()
                Me.Dispose()
            End If

            Form1.FlowLayoutPanel1.VerticalScroll.Value = Form1.FlowLayoutPanel1.VerticalScroll.Maximum


        End If

    End Sub

    Private Sub Edit_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'store the orignal string, if me is called by the edit button
        logon = TextBoxLogon.Text
        homepage = TextBoxHome.Text
        version = TextBoxVersion.Text
        folder = TextBoxFolder.Text
    End Sub
End Class